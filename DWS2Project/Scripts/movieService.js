import {movieDbPath, apiKey} from './helpers';

export default class MovieService {

	constructor() {

	}

	getGenreList() {
		return this._callMovieDb(`genre/movie/list?api_key=${apiKey}`);
	}

	_callMovieDb(suffixUrl) {
		const results = new Promise((resolve,reject) => {
			let apiCall = new XMLHttpRequest();
			
			apiCall.onreadystatechange = function() {
			    if (this.readyState == 4) {
			    	if(this.status === 200) {
			    		resolve(JSON.parse(this.response));
			    	} else {
			    		reject(this.response);
			    	}
			    }
			};
			apiCall.open("GET", `${movieDbPath}/${suffixUrl}`);
			apiCall.send();
		});
		return results;
	}

	getMovieInfo(movieId) {
		return Promise.all([this._getSimilar(movieId), this._getReviews(movieId), this._getVideos(movieId)]);
	}

	_getSimilar(movieId) {
		return this._callMovieDb(`movie/${movieId}/similar?api_key=${apiKey}`);
	}

	_getReviews(movieId) {
		return this._callMovieDb(`movie/${movieId}/reviews?api_key=${apiKey}`);
	}

	_getVideos(movieId) {
		return this._callMovieDb(`movie/${movieId}/videos?api_key=${apiKey}`);
	}

	inTheaters(page = 1) {
		return this._callMovieDb(`movie/now_playing?page=${page}&api_key=${apiKey}`);
	}

	searchMovies(page = 1, query) {
		return this._callMovieDb(`search/movie?query=${query}&page=${page}&api_key=${apiKey}`);
	}
}